import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { Home } from '../pages/home/home';
import { MostVisited } from '../pages/most-visited/most-visited';
import { Account } from '../pages/account/account';
import { Cart } from '../pages/cart/cart';
import { Category } from '../pages/category/category';
import { Electronics } from '../pages/electronics/electronics';
import { Http, JsonpModule, HttpModule } from '@angular/http';

import { Funprovider } from '../providers/funprovider';
import { Cartprovider } from '../providers/cartprovider';

@NgModule({
  declarations: [
    MyApp,
    Home,
    MostVisited,
    Account,
    Cart,
    Category,
    Electronics
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpModule,
    JsonpModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    Home,
    MostVisited,
    Account,
    Cart,
    Category,
    Electronics
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Funprovider,
    Cartprovider,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
