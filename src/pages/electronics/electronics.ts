import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Funprovider} from '../../providers/funprovider';
import { Cartprovider } from '../../providers/cartprovider';

@IonicPage()
@Component({
  selector: 'page-electronics',
  templateUrl: 'electronics.html',
})
export class Electronics {
  public mobs: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public fun: Funprovider,public cartpro: Cartprovider) {
    this.getMobileHere();

  }
  getMobileHere(){
	  this.fun.getMobile()
  	.then((res:any) => {
      this.mobs = res.electronicproducts.camera;
		console.log('res',res);
    console.log('mobs',this.mobs);
	}),(rej=> {
		console.log("error");
  	})	
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad Electronics');
  }

}
