import { NgModule } from '@angular/core';
import { IonicModule } from 'ionic-angular';
import { Electronics } from './electronics';

@NgModule({
  declarations: [
    Electronics,
  ],
  imports: [
 
  ],
  exports: [
    Electronics
  ]
})
export class ElectronicsModule {}
