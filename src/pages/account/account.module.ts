import { NgModule } from '@angular/core';
import { IonicModule } from 'ionic-angular';
import { Account } from './account';

@NgModule({
  declarations: [
    Account,
  ],
  imports: [
  
  ],
  exports: [
    Account
  ]
})
export class AccountModule {}
