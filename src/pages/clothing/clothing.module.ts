import { NgModule } from '@angular/core';
import { IonicModule } from 'ionic-angular';
import { Clothing } from './clothing';

@NgModule({
  declarations: [
    Clothing,
  ],
  imports: [
   
  ],
  exports: [
    Clothing
  ]
})
export class ClothingModule {}
