import { NgModule } from '@angular/core';
import { IonicModule } from 'ionic-angular';
import { MostVisited } from './most-visited';

@NgModule({
  declarations: [
    MostVisited,
  ],
  imports: [
    
  ],
  exports: [
    MostVisited
  ]
})
export class MostVisitedModule {}
