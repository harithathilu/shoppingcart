import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { MostVisited } from '../most-visited/most-visited';
import { Category } from '../category/category';
import { Account } from '../account/account';
import { Cart } from '../cart/cart';

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class Home {
	tab1: any;
	tab2: any;
	tab3: any;
	tab4: any;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
	  this.tab1 = MostVisited;
	  this.tab2 = Category;
	  this.tab3 = Account;
	  this.tab4 = Cart;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Home');
  }

}
